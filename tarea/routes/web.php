<?php

use App\Http\Controllers\Perro;
use App\Http\Controllers\PerroController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Node\CrapIndex;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return view('welcome');
});

Route::group(['prefix' => '/perro'], function() {
    Route::get('/',   [ PerroController::class, 'index'] )->name('todos');
    Route::get('/show/{id}',  [ PerroController::class, 'show'] )->name('ver');
    Route::get('/create/',   [ PerroController::class, 'create'] )->name('crear');
    Route::get('/edit/{id}',   [PerroController::class, 'edit'] )->name('actualizar');
    //user actions
    Route::post('/nuevo',   [ PerroController::class, 'store'] )->name('nuevoPerro');
    Route::put('/update',   [PerroController::class, 'updatePerro'] )->name('actualizarPerro');
    Route::get('/delete/{id}',   [ PerroController::class, 'destroy'] )->name('borrarPerro');




});



//Route::get('/index/', [Perro::class,index]);

//Route::group(['prefix' => '/perro'], function(){

    //Route::get('/', [PerroController::class, 'index']);
    //Route::get('/show/{id}', [PerroController::class, 'show']);

    //Route::get('/razas/create/', [PerroController::class, 'create']);
    //Route::get('/edit/{id}', [PerroController::class, 'edit']);

    /*Route::post('/store', [PerroController::class, 'store']);
    Route::put('/update', [PerroController::class, 'put']);
    //Route::delete('/delete', [PerroController::class, 'destroy']);*/

    