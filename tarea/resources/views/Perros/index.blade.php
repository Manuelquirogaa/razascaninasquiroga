@extends('layout')



@section('principal')

    <h1> {{$title}} </h1>

    <a href=" {{ route('crear') }} " type="button" class="btn btn-primary my-3"
    >Crear Raza Canina</a>

    <div align="below"><img src="/img/razas.jpg"></div>


    @empty($perros)
        No hay registros
    @endempty

    <ul>
        @foreach ($perros as $perro)
            <li>

                <a href=" {{ route('ver', $perro->id ) }} ">
                    {{$perro->name}}
                </a>
-
                <a class="link-danger" href=" {{ route('borrarPerro', $perro->id ) }} ">
                <button class="btn">
    <img src="https://w7.pngwing.com/pngs/228/54/png-transparent-logo-trademark-brand-delete-button-miscellaneous-text-trademark.png" width="30px">
</button>

                </a>

            </li>
        @endforeach

    </ul>

@endsection
