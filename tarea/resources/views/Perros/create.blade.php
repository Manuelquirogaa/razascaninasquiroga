@extends('layout')


@section('principal')

    <h1>Creando Una Nueva Raza Canina</h1>

    <form method="POST" action="{{ route('nuevoPerro') }}">
        {{ csrf_field() }}

        <div class="mb-3">
            <label for="name" class="form-label">Nombre De La Raza</label>
            <input type="text" name="nombre" class="form-control" id="name" value="{{ old('nombre') }}">

            @if ($errors->has('nombre'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('nombre') }}
                </div>
            @endif

        </div>
        <div class="mb-3">
            <label for="imgurl" class="form-label">Imagen URL</label>
            <input type="text" name="imagen" class="form-control" id="imgurl" value="{{ old('imagen') }}">

            @if ($errors->has('imagen'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('imagen') }}
                </div>
            @endif

        </div>

        <div class="mb-3">
            <label for="descripcion" class="form-label">Descripcion De La Raza</label>
            <input type="descripcion" class="form-control" id="descripcion"  name="descripcion">
            @if ($errors->has('descripcion'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('descripcion') }}
                </div>
            @endif
        </div>


        <button type="submit" class="btn btn-primary">Crear</button>
        <a href=" {{ route('todos') }} "><button class="btn">
    <img src="https://i0.wp.com/cdpic.org.do/home/wp-content/uploads/2017/11/boton-regresar.png?ssl=1" width="120px">
</button></a>   
        <br>
        <div align="center"><img src="/img/razas.jpg"></div>



    </form>


@endsection
