<?php

namespace Database\Seeders;
use App\Models\Perro;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PerrosSeeder extends Seeder
{
    
    public function run()
    {
        $Perro = new Perro;
            $Perro->name = 'Pastor aleman';
            $Perro->imgurl = 'https://www.happets.com/blog/wp-content/uploads/2020/02/pastor-aleman-alimentacion-y-cuidados-1180x838.jpg';
            $Perro->descripcion = 'El pastor alemán u ovejero alemán (en alemán: Deutscher Schäferhund) es una raza canina que proviene de Alemania. La raza es relativamente nueva, ya que su origen se remonta a 1899. Forman parte del grupo de pastoreo, ya que fueron perros desarrollados originalmente para reunir y vigilar ganado. Desde entonces, sin embargo, gracias a su fuerza, inteligencia, capacidad de entrenamiento y obediencia';
            $Perro->save();
            


        
    }
}
